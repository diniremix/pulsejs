# PulseJS
**pulseJS** is a wrapper of javascript functions for Ajax, using **JQuery**


## Requisitos
- [JQuery](https://code.jquery.com/) Version 1.8.2 o superior

## Instalacion
Clonar este repositorio y alojarlo en una carpeta conveniente de su aplicacion/sitio web

    git clone git@bitbucket.org:diniremix/pulsejs.git

Puede también descargar la versión mas reciente de **pulseJS** [por aquí](https://bitbucket.org/diniremix/pulsejs/get/master.zip)


## Configuracion
Si bien **pulseJS** viene con una configuración basica y listo para usar, por defecto se usa una configuración como la siguiente:

```javascript
pulseJS.config({});
```

ó especificar una para el manejo de su app
```javascript
pulseJS.config({
  url_base:'/api/v1/',
  token_name:'public_key',
  public_key_token:'',
  data_request:'jsonData'
});
```
y reemplazar los valores de las variables de configuración para su posterior uso:

**Ejemplo:**
```javascript
pulseJS.config({
  url_base:'/api/v1/',
  token_name:'public_key_token',
  public_key_token:'5542b31a95155e262f5df18478f42d8e',
  data_request:'jsonData'
});
```


## Uso de pulseJS
Un ejemplo de **Ajax** usando **JQuery** puede ser de la forma que sigue, utilizando la configuración del ejemplo anterior:

```javascript
$.ajax({
    url: '/api/v1/users',
	type: 'GET',
	data: {'mydata':mydata},
	dataType:'json',
	success: function(data){
		console.info(data);
	},
	error:function(data,status){
		console.error(data,status);
	}
});
```

Usando **pulseJS** tenemos:

```javascript
pulseJS.callAjax('users','GET',mydata,'json',resultCallback);
```

Donde **resultCallback** es una funcion que se llama una vez se realice la peticion a */api/v1/users* para el tratamiento de la informacion (*JSON,* como tipo de dato por defecto) que devuelva como respuesta.

**Ejemplo de la funcion **

```javascript
function resultCallback(data,status){
    console.info(data,status);
}
```


## Documentación 
La documentación de **pulseJS** puede ser encontrada en la [Wiki del proyecto](https://bitbucket.org/diniremix/pulsejs/wiki)



## Licencia
**PulseJS** es Software de código abierto [bajo la licencia **MIT**](http://opensource.org/licenses/MIT)

El texto completo de la licencia puede ser encontrado en el archivo **MIT-LICENSE.txt**


## Contacto
[Diniremix on Bitbucket](https://bitbucket.org/diniremix)

email: *diniremix [at] gmail [dot] com*

