/*! pulseJS 0.5 */
var pulseJS=(function () {
	var VERSION='0.5';
	var URL_BASE,
		AUTH_TOKEN_NAME,
		AUTH_KEY_TOKEN,
		DATA_REQUEST;

	getFormsElements=function (form) {
		var regForm = $('#'+form).serializeArray();
		var regFormObject = {};
		var cont=0;
		$.each(regForm,function(i, v) {
			regFormObject[v.name] = v.value;
		});
		return regFormObject;
	};

	validateForms=function (form) {
		var st=true;
		$.each(form,function(i, v) {
			if((v=="")||(v==null)){
				st= false;
			}
		});
		return st;
	};

	redirect=function(page){
		document.location=page;
	};

	asJson=function (obj){
        var jsonForm = JSON.stringify(obj,null,2);
        return jsonForm;
    };

    isJson=function (cad) {
		try{
			var json = JSON.parse(cad);
		}
		catch(e){
			return false;
		}
		return true;
	};

	ajax=function(url,method,data,dataType,callback){
		var myurl,
			mydata;

		myurl=URL_BASE+url;
		if(data === null && typeof data === "object"){
			console.warn("data es null",data);
		}else if( !isNaN(data) || (typeof data === "string") ){
			myurl+='/'+data;
		}else if(typeof data === "object"){
			data=pulseJS.asJson(data);
			if(pulseJS.isJson(data)){
				mydata=data;
			}
			if(DATA_REQUEST != "" ){
				var requestData={};
				requestData[DATA_REQUEST]=mydata;
				console.log("json_request", requestData);
				mydata= requestData;
			}
		}

		$.ajax({
			url: myurl,
			type: method ? method : 'GET',
			beforeSend: function (request){
				request.setRequestHeader(AUTH_TOKEN_NAME, AUTH_KEY_TOKEN);
			},
			data: mydata? mydata: {},
			dataType: dataType ? dataType : 'json',
			success: function(data){
				if(data.status==200){
					(typeof(callback) === 'function') ? callback({status:data.status,response:data.data}) : '';
				}else{
					(typeof(callback) === 'function') ? callback({status:data.status,message:data.message}) : '';
				}
			},
			error:function(data,status){
				var response={
						status:data.status?data.status:'',
						statusText:data.statusText?data.statusText:'',
						responseText:data.responseText?data.responseText:''
					};
				(typeof(callback) === 'function') ? callback(response,status) : '';
			}
		});
	};

	config=function(configuration){		
		URL_BASE=configuration.url_base? configuration.url_base : "api/v1/";
		AUTH_TOKEN_NAME=configuration.token_name? configuration.token_name : "public_key";
		AUTH_KEY_TOKEN=configuration.public_key_token? configuration.public_key_token : "";
		DATA_REQUEST=configuration.data_request? configuration.data_request : "";

		console.info("Welcome to pulseJS Version:",VERSION);
		console.info("pulseJS configuration",URL_BASE,AUTH_TOKEN_NAME,AUTH_KEY_TOKEN,DATA_REQUEST);
	};

	return {
		getFormsElements:getFormsElements,
		validateForms:validateForms,
		redirect:redirect,
		asJson:asJson,
		isJson:isJson,
		callAjax:ajax,
		config:config
	};
})();
